package ru.hybrid.simpleble;

import java.util.HashMap;

/**
 * Created by p.kuraev on 12.07.2017.
 */

public class SampleGattAttribute {
    private static HashMap<String, String> attributes = new HashMap();
    public static String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";


    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }

    static {
        // Sample Services.
        attributes.put("0000180d-0000-1000-8000-00805f9b34fb", "Heart Rate Service");
        attributes.put("0000180a-0000-1000-8000-00805f9b34fb", "Device Information Service");
        // Sample Characteristics.

        attributes.put(HEART_RATE_MEASUREMENT, "Heart Rate Measurement");
        attributes.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name String");

        attributes.put("00002a43-0000-1000-8000-00805f9b34fb","Alert Category ID");
        attributes.put("00002a42-0000-1000-8000-00805f9b34fb","Alert Category ID Bit Mask");
        attributes.put("00002a06-0000-1000-8000-00805f9b34fb","Alert Level");
        attributes.put("00002a44-0000-1000-8000-00805f9b34fb","Alert Notification Control Point");
        attributes.put("00002a3f-0000-1000-8000-00805f9b34fb","Alert Status");
        attributes.put("00002a01-0000-1000-8000-00805f9b34fb","Appearance");
        attributes.put("00002a19-0000-1000-8000-00805f9b34fb","Battery Level");
        attributes.put("00002a49-0000-1000-8000-00805f9b34fb","Blood Pressure Feature");
        attributes.put("00002a35-0000-1000-8000-00805f9b34fb","Blood Pressure Measurement");
        attributes.put("00002a38-0000-1000-8000-00805f9b34fb","Body Sensor Location");
        attributes.put("00002a22-0000-1000-8000-00805f9b34fb","Boot Keyboard Input Report");
        attributes.put("00002a32-0000-1000-8000-00805f9b34fb","Boot Keyboard Output Report");
        attributes.put("00002a33-0000-1000-8000-00805f9b34fb","Boot Mouse Input Report");
        attributes.put("00002a5c-0000-1000-8000-00805f9b34fb","CSC Feature");
        attributes.put("00002a5b-0000-1000-8000-00805f9b34fb","CSC Measurement");
        attributes.put("00002a2b-0000-1000-8000-00805f9b34fb","Current Time");
        attributes.put("00002a66-0000-1000-8000-00805f9b34fb","Cycling Power Control Point");
        attributes.put("00002a65-0000-1000-8000-00805f9b34fb","Cycling Power Feature");
        attributes.put("00002a63-0000-1000-8000-00805f9b34fb","Cycling Power Measurement");
        attributes.put("00002a64-0000-1000-8000-00805f9b34fb","Cycling Power Vector");
        attributes.put("00002a08-0000-1000-8000-00805f9b34fb","Date Time");
        attributes.put("00002a0a-0000-1000-8000-00805f9b34fb","Day Date Time");
        attributes.put("00002a09-0000-1000-8000-00805f9b34fb","Day of Week");
        attributes.put("00002a00-0000-1000-8000-00805f9b34fb","Device Name");
        attributes.put("00002a0d-0000-1000-8000-00805f9b34fb","DST Offset");
        attributes.put("00002a0c-0000-1000-8000-00805f9b34fb","Exact Time 256");
        attributes.put("00002a26-0000-1000-8000-00805f9b34fb","Firmware Revision String");
        attributes.put("00002a51-0000-1000-8000-00805f9b34fb","Glucose Feature");
        attributes.put("00002a18-0000-1000-8000-00805f9b34fb","Glucose Measurement");
        attributes.put("00002a34-0000-1000-8000-00805f9b34fb","Glucose Measure Context");
        attributes.put("00002a27-0000-1000-8000-00805f9b34fb","Hardware Revision String");
        attributes.put("00002a39-0000-1000-8000-00805f9b34fb","Heart Rate Control Point");
        attributes.put("00002a37-0000-1000-8000-00805f9b34fb","Heart Rate Measurement");
        attributes.put("00002a4c-0000-1000-8000-00805f9b34fb","HID Control Point");
        attributes.put("00002a4a-0000-1000-8000-00805f9b34fb","HID Information");
        attributes.put("00002a2a-0000-1000-8000-00805f9b34fb","IEEE 11073-20601 Regulatory Certification Data List");
        attributes.put("00002a36-0000-1000-8000-00805f9b34fb","Intermediate Cuff Pressure");
        attributes.put("00002a1e-0000-1000-8000-00805f9b34fb","Intermediate Temperature");
        attributes.put("00002a6b-0000-1000-8000-00805f9b34fb","LN Control Point");
        attributes.put("00002a6a-0000-1000-8000-00805f9b34fb","LN Feature");
        attributes.put("00002a0f-0000-1000-8000-00805f9b34fb","Local Time Information");
        attributes.put("00002a67-0000-1000-8000-00805f9b34fb","Location And Speed");
        attributes.put("00002a29-0000-1000-8000-00805f9b34fb","Manufacturer Name String");
        attributes.put("00002a21-0000-1000-8000-00805f9b34fb","Measurement Interval");
        attributes.put("00002a24-0000-1000-8000-00805f9b34fb","Model Number String");
        attributes.put("00002a68-0000-1000-8000-00805f9b34fb","Navigation");
        attributes.put("00002a46-0000-1000-8000-00805f9b34fb","New Alert");
        attributes.put("00002a04-0000-1000-8000-00805f9b34fb","Peripheral Preferred Connection Parameters");
        attributes.put("00002a02-0000-1000-8000-00805f9b34fb","Peripheral Privacy Flag");
        attributes.put("00002a50-0000-1000-8000-00805f9b34fb","PnP ID");
        attributes.put("00002a69-0000-1000-8000-00805f9b34fb","Position Quality");
        attributes.put("00002a4e-0000-1000-8000-00805f9b34fb","Protocol Mode");
        attributes.put("00002a03-0000-1000-8000-00805f9b34fb","Reconnection Address");
        attributes.put("00002a52-0000-1000-8000-00805f9b34fb","Record Access Control Point");
        attributes.put("00002a14-0000-1000-8000-00805f9b34fb","Reference Time Information");
        attributes.put("00002a4d-0000-1000-8000-00805f9b34fb","Report");
        attributes.put("00002a4b-0000-1000-8000-00805f9b34fb","Report Map");
        attributes.put("00002a40-0000-1000-8000-00805f9b34fb","Ringer Control Point");
        attributes.put("00002a41-0000-1000-8000-00805f9b34fb","Ringer Setting");
        attributes.put("00002a54-0000-1000-8000-00805f9b34fb","RSC Feature");
        attributes.put("00002a53-0000-1000-8000-00805f9b34fb","RSC Measurement");
        attributes.put("00002a55-0000-1000-8000-00805f9b34fb","SC Control Point");
        attributes.put("00002a4f-0000-1000-8000-00805f9b34fb","Scan Interval Window");
        attributes.put("00002a31-0000-1000-8000-00805f9b34fb","Scan Refresh");
        attributes.put("00002a5d-0000-1000-8000-00805f9b34fb","Sensor Location");
        attributes.put("00002a25-0000-1000-8000-00805f9b34fb","Serial Number String");
        attributes.put("00002a05-0000-1000-8000-00805f9b34fb","Service Changed");
        attributes.put("00002a28-0000-1000-8000-00805f9b34fb","Software Revision String");
        attributes.put("00002a47-0000-1000-8000-00805f9b34fb","Supported New Alert Category");
        attributes.put("00002a48-0000-1000-8000-00805f9b34fb","Supported Unread Alert Category");
        attributes.put("00002a23-0000-1000-8000-00805f9b34fb","System ID");
        attributes.put("00002a1c-0000-1000-8000-00805f9b34fb","Temperature Measurement");
        attributes.put("00002a1d-0000-1000-8000-00805f9b34fb","Temperature Type");
        attributes.put("00002a12-0000-1000-8000-00805f9b34fb","Time Accuracy");
        attributes.put("00002a13-0000-1000-8000-00805f9b34fb","Time Source");
        attributes.put("00002a16-0000-1000-8000-00805f9b34fb","Time Update Control Point");
        attributes.put("00002a17-0000-1000-8000-00805f9b34fb","Time Update State");
        attributes.put("00002a11-0000-1000-8000-00805f9b34fb","Time with DST");
        attributes.put("00002a0e-0000-1000-8000-00805f9b34fb","Time Zone");
        attributes.put("00002a07-0000-1000-8000-00805f9b34fb","Tx Power Level");
        attributes.put("00002a45-0000-1000-8000-00805f9b34fb","Unread Alert Status");
        attributes.put("00002a7e-0000-1000-8000-00805f9b34fb","Aerobic Heart Rate Lower Limit");
        attributes.put("00002a84-0000-1000-8000-00805f9b34fb","Aerobic Heart Rate Uppoer Limit");
        attributes.put("00002a80-0000-1000-8000-00805f9b34fb","Age");
        attributes.put("00002a5a-0000-1000-8000-00805f9b34fb","Aggregate Input");
        attributes.put("00002a81-0000-1000-8000-00805f9b34fb","Anaerobic Heart Rate Lower Limit");
        attributes.put("00002a82-0000-1000-8000-00805f9b34fb","Anaerobic Heart Rate Upper Limit");
        attributes.put("00002a83-0000-1000-8000-00805f9b34fb","Anaerobic Threshold");
        attributes.put("00002a58-0000-1000-8000-00805f9b34fb","Analog Input");
        attributes.put("00002a89-0000-1000-8000-00805f9b34fb","Analog Output");
        attributes.put("00002a73-0000-1000-8000-00805f9b34fb","Apparent Wind Direction");
        attributes.put("00002a72-0000-1000-8000-00805f9b34fb","Apparent Wind Speed");
        attributes.put("00002a9b-0000-1000-8000-00805f9b34fb","Body Composition Feature");
        attributes.put("00002a9c-0000-1000-8000-00805f9b34fb","Body Composition Measurement");
        attributes.put("00002a99-0000-1000-8000-00805f9b34fb","Database Change Increment");
        attributes.put("00002a85-0000-1000-8000-00805f9b34fb","Date of Birth");
        attributes.put("00002a86-0000-1000-8000-00805f9b34fb","Date of Threshold Assessment");
        attributes.put("00002a7d-0000-1000-8000-00805f9b34fb","Descriptor Value Changed");
        attributes.put("00002a7b-0000-1000-8000-00805f9b34fb","Dew Point");
        attributes.put("00002a56-0000-1000-8000-00805f9b34fb","Digital Input");
        attributes.put("00002a57-0000-1000-8000-00805f9b34fb","Digital Output");
        attributes.put("00002a6c-0000-1000-8000-00805f9b34fb","Elevation");
        attributes.put("00002a87-0000-1000-8000-00805f9b34fb","Email Address");
        attributes.put("00002a0b-0000-1000-8000-00805f9b34fb","Exact Time 100");
        attributes.put("00002a88-0000-1000-8000-00805f9b34fb","Fat Burn Heart Rate Lower Limit");
        attributes.put("00002a89-0000-1000-8000-00805f9b34fb","Fat Burn Heart Rate Upper Limit");
        attributes.put("00002a8a-0000-1000-8000-00805f9b34fb","First Name");
        attributes.put("00002a8b-0000-1000-8000-00805f9b34fb","Five Zone Heart Rate Limits");
        attributes.put("00002a8c-0000-1000-8000-00805f9b34fb","Gender");
        attributes.put("00002a74-0000-1000-8000-00805f9b34fb","Gust Factor");
        attributes.put("00002a8d-0000-1000-8000-00805f9b34fb","Heart Rate Max");
        attributes.put("00002a7a-0000-1000-8000-00805f9b34fb","Heat Index");
        attributes.put("00002a8e-0000-1000-8000-00805f9b34fb","Height");
        attributes.put("00002a8f-0000-1000-8000-00805f9b34fb","Hip Circumference");
        attributes.put("00002a6f-0000-1000-8000-00805f9b34fb","Humidity");
        attributes.put("00002a77-0000-1000-8000-00805f9b34fb","Irradiance");
        attributes.put("00002a90-0000-1000-8000-00805f9b34fb","Last Name");
        attributes.put("00002a91-0000-1000-8000-00805f9b34fb","Maximum Recommended Heart Rate");
        attributes.put("00002a3e-0000-1000-8000-00805f9b34fb","Network Availability");
        attributes.put("00002a75-0000-1000-8000-00805f9b34fb","Pollen Concentration");
        attributes.put("00002a6d-0000-1000-8000-00805f9b34fb","Pressure");
        attributes.put("00002a78-0000-1000-8000-00805f9b34fb","Rainfall");
        attributes.put("00002a92-0000-1000-8000-00805f9b34fb","Resting Heart Rate");
        attributes.put("00002a3c-0000-1000-8000-00805f9b34fb","Scientific Temperature in Celsius");
        attributes.put("00002a10-0000-1000-8000-00805f9b34fb","Secondary Time Zone");
        attributes.put("00002a93-0000-1000-8000-00805f9b34fb","Sport Type for Aerobic and Anaerobic Thresholds");
        attributes.put("00002a3d-0000-1000-8000-00805f9b34fb","String");
        attributes.put("00002a6e-0000-1000-8000-00805f9b34fb","Temperature");
        attributes.put("00002a1f-0000-1000-8000-00805f9b34fb","Temperature in Celsius");
        attributes.put("00002a20-0000-1000-8000-00805f9b34fb","Temperature in Fahrenheit");
        attributes.put("00002a94-0000-1000-8000-00805f9b34fb","Three Zone Heart Rate Limits");
        attributes.put("00002a15-0000-1000-8000-00805f9b34fb","Time Broadcast");
        attributes.put("00002a7c-0000-1000-8000-00805f9b34fb","Trend");
        attributes.put("00002a71-0000-1000-8000-00805f9b34fb","True Wind Direction");
        attributes.put("00002a70-0000-1000-8000-00805f9b34fb","True Wind Speed");
        attributes.put("00002a95-0000-1000-8000-00805f9b34fb","Two Zone Heart Rate Limit");
        attributes.put("00002a9f-0000-1000-8000-00805f9b34fb","User Control Point");
        attributes.put("00002a9a-0000-1000-8000-00805f9b34fb","User Index");
        attributes.put("00002a76-0000-1000-8000-00805f9b34fb","UV Index");
        attributes.put("00002a96-0000-1000-8000-00805f9b34fb","VO2 Max");
        attributes.put("00002a97-0000-1000-8000-00805f9b34fb","Waist Circumference");
        attributes.put("00002a98-0000-1000-8000-00805f9b34fb","Weight");
        attributes.put("00002a9d-0000-1000-8000-00805f9b34fb","Weight Measurement");
        attributes.put("00002a9e-0000-1000-8000-00805f9b34fb","Weight Scale Feature");
        attributes.put("00002a79-0000-1000-8000-00805f9b34fb","Wind Chill");
        attributes.put("00002a1b-0000-1000-8000-00805f9b34fb","Battery Level State");
        attributes.put("00002a1a-0000-1000-8000-00805f9b34fb","Battery Power State");
        attributes.put("00002a2d-0000-1000-8000-00805f9b34fb","Latitude");
        attributes.put("00002a2e-0000-1000-8000-00805f9b34fb","Longitude");
        attributes.put("00002a2f-0000-1000-8000-00805f9b34fb","Position 2D");
        attributes.put("00002a30-0000-1000-8000-00805f9b34fb","Position 3D");
        attributes.put("00002a5f-0000-1000-8000-00805f9b34fb","Pulse Oximetry Continuous Measurement");
        attributes.put("00002a62-0000-1000-8000-00805f9b34fb","Pulse Oximetry Control Point");
        attributes.put("00002a61-0000-1000-8000-00805f9b34fb","Pulse Oximetry Features");
        attributes.put("00002a60-0000-1000-8000-00805f9b34fb","Pulse Oximetry Pulsatile Event");
        attributes.put("00002a5e-0000-1000-8000-00805f9b34fb","Pulse Oximetry Spot-Check Measurement");
        attributes.put("00002a52-0000-1000-8000-00805f9b34fb","Record Access Control Point (Test Version)");
        attributes.put("00002a3a-0000-1000-8000-00805f9b34fb","Removable");
        attributes.put("00002a3b-0000-1000-8000-00805f9b34fb","Service Required");
        attributes.put("0000ffe1-0000-1000-8000-00805f9b34fb","TI SensorTag Keys Data");
        attributes.put("f000aa01-0451-4000-b000-000000000000","TI SensorTag Infrared Temperature Data");
        attributes.put("f000aa02-0451-4000-b000-000000000000","TI SensorTag Infrared Temperature On/Off");
        attributes.put("f000aa03-0451-4000-b000-000000000000","TI SensorTag Infrared Temperature Sample Rate");
        attributes.put("f000aa11-0451-4000-b000-000000000000","TI SensorTag Accelerometer Data");
        attributes.put("f000aa12-0451-4000-b000-000000000000","TI SensorTag Accelerometer On/Off");
        attributes.put("f000aa13-0451-4000-b000-000000000000","TI SensorTag Accelerometer Sample Rate");
        attributes.put("f000aa21-0451-4000-b000-000000000000","TI SensorTag Humidity Data");
        attributes.put("f000aa22-0451-4000-b000-000000000000","TI SensorTag Humidity On/Off");
        attributes.put("f000aa23-0451-4000-b000-000000000000","TI SensorTag Humidity Sample Rate");
        attributes.put("f000aa31-0451-4000-b000-000000000000","TI SensorTag Magnometer Data");
        attributes.put("f000aa32-0451-4000-b000-000000000000","TI SensorTag Magnometer On/Off");
        attributes.put("f000aa33-0451-4000-b000-000000000000","TI SensorTag Magnometer Sample Rate");
        attributes.put("f000aa41-0451-4000-b000-000000000000","TI SensorTag Barometer Data");
        attributes.put("f000aa42-0451-4000-b000-000000000000","TI SensorTag Barometer On/Off");
        attributes.put("f000aa43-0451-4000-b000-000000000000","TI SensorTag Barometer Calibration");
        attributes.put("f000aa44-0451-4000-b000-000000000000","TI SensorTag Barometer Sample Rate");
        attributes.put("f000aa51-0451-4000-b000-000000000000","TI SensorTag Gyroscope Data");
        attributes.put("f000aa52-0451-4000-b000-000000000000","TI SensorTag Gyroscope On/Off");
        attributes.put("f000aa53-0451-4000-b000-000000000000","TI SensorTag Gyroscope Sample Rate");
        attributes.put("f000aa61-0451-4000-b000-000000000000","TI SensorTag Test Data");
        attributes.put("f000aa62-0451-4000-b000-000000000000","TI SensorTag Test Configuration");
        attributes.put("f000c0e1-0451-4000-b000-000000000000","TI Serial Port Data");
        attributes.put("f000c0e2-0451-4000-b000-000000000000","TI Serial Port Status");
        attributes.put("f000c0e3-0451-4000-b000-000000000000","TI Serial Port Config");
        attributes.put("f000ccc1-0451-4000-b000-000000000000","TI SensorTag Connection Parameters");
        attributes.put("f000ccc2-0451-4000-b000-000000000000","TI SensorTag Connection Request Parameters");
        attributes.put("f000ccc3-0451-4000-b000-000000000000","TI SensorTag Connection Request Disconnect");
        attributes.put("f000ffc1-0451-4000-b000-000000000000","TI SensorTag OAD Image Identify");
        attributes.put("f000ffc2-0451-4000-b000-000000000000","TI SensorTag OAD Image Block");

        attributes.put("713d0001-503e-4c75-ba94-3148f18d941e","RedBearLabs Biscuit Read");
        attributes.put("713d0002-503e-4c75-ba94-3148f18d941e","RedBearLabs Biscuit TX_DATA_CHAR_UUID Notify");
        attributes.put("713d0003-503e-4c75-ba94-3148f18d941e","RedBearLabs Biscuit RX_DATA_CHAR_UUID WriteWithoutResponse");
        attributes.put("713d0004-503e-4c75-ba94-3148f18d941e","RedBearLabs Biscuit BAUDRATE_CHAR_UUID WriteWithoutResponse");
        attributes.put("713d0005-503e-4c75-ba94-3148f18d941e","RedBearLabs Biscuit DEV_NAME_CHAR_UUID Read");

        attributes.put("713d0006-503e-4c75-ba94-3148f18d941e","RedBearLabs Biscuit VERSION_CHAR_UUID unknown");
        attributes.put("713d0007-503e-4c75-ba94-3148f18d941e","RedBearLabs Biscuit TX_POWER_CHAR_UUID unknown");
    }


}