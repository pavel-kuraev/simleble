package ru.hybrid.simpleble;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by p.kuraev on 11.07.2017.
 */

public class BluetoothDevicesAdapter extends ArrayAdapter<BluetoothDevice> implements ListAdapter {
    private ArrayList<BluetoothDevice> listDevices;

    public BluetoothDevicesAdapter(@NonNull Context context, @NonNull ArrayList<BluetoothDevice> btDevices) {
        super(context, 0, btDevices);
        listDevices = btDevices;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.bt_device_list_elment, parent, false);
            TextView name = (TextView)convertView.findViewById(R.id.device_name);
            TextView address = (TextView)convertView.findViewById(R.id.device_address);
            BluetoothDevice device = getItem(position);
            name.setText(device.getName());
            address.setText(device.getAddress());
        }
        return convertView;
    }

    @Override
    public void add(@Nullable BluetoothDevice object) {
        if (!listDevices.contains(object)){
            super.add(object);
        }

    }
}
