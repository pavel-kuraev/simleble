package ru.hybrid.simpleble;

import android.util.Log;

/**
 * Created by p.kuraev on 19.07.2017.
 */

public class BTMessage {
    static String LOG_TAG = BTMessage.class.getSimpleName();
    static int ADDRESS_LENGTH_BYTES = 1;
    static int DATA_LENGTH_BYTES = 4;
    static int CRC16_LENGTH_BYTES = 2;

    private byte[] rawData;
    private int address;
    private float data;
    public int messageCrc16;


    public BTMessage(byte[] rawData, int address, float data, int crc16){
        this.rawData = rawData;
        this.address = address;
        this.data = data;
        this.messageCrc16 = crc16;
    }

    public int getAddress() {
        return address;
    }

    public void setData(float data){
        data = data;
    }

    public float getData() {
        return data;
    }

    public int getCrcOfData() {
        Crc16Modbus crcModbus = new Crc16Modbus();
        for (int i = 0; i < ADDRESS_LENGTH_BYTES + DATA_LENGTH_BYTES; i++){
            crcModbus.update(this.rawData[i]);
        }
        int crcOfMessage = (int)crcModbus.getValue();
        Log.i(LOG_TAG, "Crc16 of data " + Integer.toString(crcOfMessage));
        return crcOfMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (address == ((BTMessage)o).address ) return true;
        return false;
    }






}
