package ru.hybrid.simpleble;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class DeviceInfoActivity extends AppCompatActivity {
    public String UUID_SPP_SERIAL_PORT = "f000c0e1-0451-4000-b000-000000000000";

    public static String LOG_TAG = DeviceInfoActivity.class.getSimpleName();
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    public static final String EXTRAS_DEVICE_NAME = "device_name";
    public static final String EXTRAS_DEVICE_ADDRESS = "device_address";
    private boolean mConnected;
    private String mDeviceName;
    private String mDeviceAddress;

    //     UI section
    private TextView mTextViewDeviceName;
    private TextView mTextViewDeviceAddress;
    private  TextView mDataField;
    private  TextView mConnectionStatus;
    private  TextView mNotificationStatus;
    private ListView mListMessages;
    private Button mClearButton;

    private BluetoothGattCharacteristic mOurServiceCharacteristic;
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private BTMessageAdapter mMessagesAdapter;



    //    Service section
    private BluetoothService mBluetoothLeService;
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(LOG_TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            Log.i(LOG_TAG, "Init bt connection first start");
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                Log.e(LOG_TAG, "Connected device");
                mConnectionStatus.setText(R.string.connected);

                invalidateOptionsMenu();
            } else if (BluetoothService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                Log.e(LOG_TAG, "Disconnected device");
                mConnectionStatus.setText(R.string.no_connection);
                clearData();
            } else if (BluetoothService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                Log.e(LOG_TAG, "Discovered device");
                mConnectionStatus.setText(R.string.discovered);
                // Прологгировал все доступные сервисы. Нашел serial
                logGattServicesAndSearchSerialDataPort(mBluetoothLeService.getSupportedGattServices());
                enableNotificationForSerialPort();
            } else if (BluetoothService.ACTION_DATA_AVAILABLE.equals(action)) {
                Log.e(LOG_TAG, "Notification available");
                displayData(intent.getByteArrayExtra(BluetoothService.EXTRA_DATA));
            }
        }
    };


    private void displayData(byte[] data){
        if (data != null) {
            BTMessage message = BTMessageFactory.getBtMessage(data);
            if (message != null){
                this.mMessagesAdapter.add(message);
            }
//            Просто показать в тектовом поле последнее сообщение
            StringBuilder stringBuilderMessageBytes = new StringBuilder();
            for (byte bb: data){
                stringBuilderMessageBytes.append(String.format("%02X ", bb));
            }
            mDataField.setText(stringBuilderMessageBytes.toString());
        }
    }



    private void clearData(){
        mDataField.setText(R.string.no_data);
        mMessagesAdapter.clear();
    }

    private void logGattServicesAndSearchSerialDataPort(List<BluetoothGattService> gattServices) {
        if (gattServices == null) {
            Log.w(LOG_TAG, "Не найдено сервисов");
            return;
        }
        String unknownServiceString = "unknown service";
        String unknownCharaString = "unknowm_chara";
        String uuid = null;
        for (BluetoothGattService gattService : gattServices) {
            Integer type = gattService.getType();
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(LIST_NAME, SampleGattAttribute.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            ArrayList<HashMap<String, String>> gattCharacteristicGroupData = new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas = new ArrayList<BluetoothGattCharacteristic>();
            Log.i(LOG_TAG, "Characteristics for servce");
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {

                charas.add(gattCharacteristic);

                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();

                if (uuid.equals(UUID_SPP_SERIAL_PORT)){
                    Log.i(LOG_TAG, "Find Spp serial  uuid");
                    mOurServiceCharacteristic = gattCharacteristic;
                }
                mBluetoothLeService.setCharacteristicNotification(gattCharacteristic, true);
                currentCharaData.put(LIST_NAME, SampleGattAttribute.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
                Log.i(LOG_TAG, "Permissions " + gattCharacteristic.getPermissions());
                Log.i(LOG_TAG, "Write type " + gattCharacteristic.getWriteType());
                Log.i(LOG_TAG, "UUID " + uuid);
                Log.i(LOG_TAG, "Lookup for uuid " + SampleGattAttribute.lookup(uuid, unknownCharaString));

            }
        }

    }

    private void enableNotificationForSerialPort(){
        if (mOurServiceCharacteristic != null){
            final int charaProp = mOurServiceCharacteristic.getProperties();

            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                // If there is an active notification on a characteristic, clear
                // it first so it doesn't update the data field on the user interface.
                Log.w(LOG_TAG, "Disable charakteristic");
//                        mButtonEnableNot.setText(R.string.notification_no);
                clearData();
                if (mNotifyCharacteristic != null) {
                    mBluetoothLeService.setCharacteristicNotification(
                            mNotifyCharacteristic, false);
                    mNotifyCharacteristic = null;
                    mNotificationStatus.setText(R.string.notification_no);
                }
                mBluetoothLeService.readCharacteristic(mOurServiceCharacteristic);
            }
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                Log.w(LOG_TAG, "Enable charakteristic");
                mNotifyCharacteristic = mOurServiceCharacteristic;
                mBluetoothLeService.setCharacteristicNotification(mOurServiceCharacteristic, true);
                BluetoothGattDescriptor descriptor2 = mOurServiceCharacteristic.getDescriptor(
                        UUID.fromString(SampleGattAttribute.CLIENT_CHARACTERISTIC_CONFIG));
                descriptor2.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                mBluetoothLeService.mBluetoothGatt.writeDescriptor(descriptor2);
                mNotificationStatus.setText(R.string.notification_yes);
            }
        }
        else{
            mNotificationStatus.setText("Serial port not init");
            Toast.makeText(this, "Serial data port not init", Toast.LENGTH_SHORT).show();
            Log.w(LOG_TAG, "Serial data port not init");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_info);
        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        mTextViewDeviceName = (TextView)findViewById(R.id.device_name);
        mTextViewDeviceAddress = (TextView)findViewById(R.id.device_address);
        mDataField = (TextView)findViewById(R.id.data_field);
        mConnectionStatus = (TextView)findViewById(R.id.connection_staus);
        mNotificationStatus= (TextView)findViewById(R.id.notif_staus);
        mListMessages = (ListView)findViewById(R.id.messages_listview);
        mClearButton = (Button)findViewById(R.id.clear_button);

        mTextViewDeviceName.setText(mDeviceName);
        mTextViewDeviceAddress.setText(mDeviceAddress);
        mMessagesAdapter = new BTMessageAdapter(this, new ArrayList<BTMessage>());
        mListMessages.setAdapter(mMessagesAdapter);

        Intent gattServiceIntent = new Intent(this, BluetoothService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearData();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(LOG_TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

}
