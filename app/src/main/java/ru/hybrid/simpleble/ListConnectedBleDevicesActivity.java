package ru.hybrid.simpleble;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

public class ListConnectedBleDevicesActivity extends AppCompatActivity {
    //    Logging
    private static String LOG_TAG = ListConnectedBleDevicesActivity.class.getSimpleName();
    //    Intents requests code
    private static final int REQUEST_ENABLE_BT = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    private static final long SCAN_PERIOD = 10000;
//    Visual interface
    ListView listBleDevices;
    SwipeRefreshLayout mSwiper;


    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner scanner;
    private BluetoothDevicesAdapter mScannedDeviceAdapter;
    private boolean mScanningState;
    private Handler mDelayStopBleScanHandler;


   private ScanCallback onScanLeBluetoothDevicesNewApinew;
   private BluetoothAdapter.LeScanCallback mLeScanCallbackOldVers = new BluetoothAdapter.LeScanCallback() {
       @Override
       public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
           Log.i(LOG_TAG, "Scan_result < kitkat");
           mScannedDeviceAdapter.add(device);
           if (device.getName() != null) {
               Log.i(LOG_TAG, device.getName());
           } else {
               Log.i(LOG_TAG, "Device name undefined");
           }
           Log.i(LOG_TAG, device.getAddress());
       }
   };

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private ScanCallback getscanCallback(){
        if (onScanLeBluetoothDevicesNewApinew == null){
            onScanLeBluetoothDevicesNewApinew =  new ScanCallback() {
                                @Override
                public void onScanFailed(int errorCode) {
                    super.onScanFailed(errorCode);
                    Log.i(LOG_TAG, "Scan_result > kitkat. Fail!!!!. Error code " + errorCode);
                }

                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    super.onScanResult(callbackType, result);
                    Log.i(LOG_TAG, "Scan_result > kitkat");
                    BluetoothDevice device = result.getDevice();
                    mScannedDeviceAdapter.add(device);
                    if (device.getName() != null) {
                        Log.i(LOG_TAG, device.getName());
                    } else {
                        Log.i(LOG_TAG, "Device name undefined");
                    }
                    Log.i(LOG_TAG, device.getAddress());
                }
            };
        }
        return onScanLeBluetoothDevicesNewApinew;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_connected_ble_devices);

        mDelayStopBleScanHandler = new Handler();
        listBleDevices = (ListView)findViewById(R.id.devices_list);
        listBleDevices.setEmptyView(findViewById(R.id.no_devices));
        mSwiper = (SwipeRefreshLayout)findViewById(R.id.swipe);


        mScannedDeviceAdapter = new BluetoothDevicesAdapter(this, new ArrayList<BluetoothDevice>());
        listBleDevices.setAdapter(mScannedDeviceAdapter);
        listBleDevices.setOnItemClickListener(onDeviceClickListener);


        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        startScanByVersions();
        mSwiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mScannedDeviceAdapter.clear();
                startScanByVersions();
            }
        });
    }

    AdapterView.OnItemClickListener onDeviceClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            BluetoothDevice device = mScannedDeviceAdapter.getItem(position);
            if (device == null){
                Log.w(LOG_TAG, "Get null device from adapter");
                return;
            }
            final Intent intent = new Intent(getApplicationContext(), DeviceInfoActivity.class);
            intent.putExtra(DeviceInfoActivity.EXTRAS_DEVICE_NAME, device.getName());
            intent.putExtra(DeviceInfoActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
            if (mScanningState) {
                mScanningState = false;
                stopScanByVersion();
            }
            startActivity(intent);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(LOG_TAG, "Result of activete BT  " + Integer.toString(resultCode));
        if (requestCode == REQUEST_ENABLE_BT){
            switch (resultCode){
                case RESULT_OK:
                    Log.i(LOG_TAG, "Успешно запущен BT");
                    startScanByVersions();
                    break;
                default:
                    Log.i(LOG_TAG, "BT не запущен. Код " + Integer.toString(resultCode));
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("BT не работает");
                    builder.setMessage("Включите Bluetooth");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(LOG_TAG, "coarse location permission granted");
                    startScanByVersions();
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Функцонал ограничен");
                    builder.setMessage("Без соответствующих разрешений приложение работать не будет");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.show();
                }
                return;
            }
        }
    }

    public void showScanProgress(boolean is_show){
        if (is_show){
            mSwiper.setRefreshing(true);
        }
        else{
            mSwiper.setRefreshing(false);
        }
    }

    public  void startScanByVersions(){
       if (!isBtAdapterEnabled())
            return;
       if (!isCheckAppPermissions()){
           return;
       }
       showScanProgress(true);
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
           scanLeDeviceNewApi(true);
       }
       else{
           scanLeDeviceOldApi(true);
       }
    }

    public void stopScanByVersion(){
        showScanProgress(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scanner.stopScan(getscanCallback());
        }
        else{
            mBluetoothAdapter.stopLeScan(mLeScanCallbackOldVers);
        }
    }

    private boolean isCheckAppPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                return false;
            }
            return true;
        }
        return true;
    }

    private boolean isBtAdapterEnabled(){
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Log.i(LOG_TAG, "Bluetooth выключен");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            return false;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void scanLeDeviceNewApi(final boolean enable){
        if (enable) {
            mDelayStopBleScanHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanningState = false;
                    Log.i(LOG_TAG, "Stop Ble Scan TIMEOUT > Kitkat");
                    stopScanByVersion();
                }
            }, SCAN_PERIOD);
            Log.i(LOG_TAG, "Start Ble Scan version > KitKat");
            mScanningState = true;
            scanner = mBluetoothAdapter.getBluetoothLeScanner();
            scanner.startScan(getscanCallback());
        } else {
            Log.i(LOG_TAG, "Stop Ble Scan. > KitKat");
            mScanningState = false;
            stopScanByVersion();
        }
    }

    private void scanLeDeviceOldApi(final boolean enable){
        if (enable) {
            mDelayStopBleScanHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanningState = false;
                    Log.i(LOG_TAG, "Stop Ble Scan TIMEOUT < Kitkat");
                    stopScanByVersion();
                }
            }, SCAN_PERIOD);
            Log.i(LOG_TAG, "Start Ble Scan version < KitKat");
            mScanningState = true;
            mBluetoothAdapter.startLeScan(mLeScanCallbackOldVers);
        } else {
            Log.i(LOG_TAG, "Stop Ble Scan. < KitKat");
            mScanningState = false;
            stopScanByVersion();
        }
    }
}
