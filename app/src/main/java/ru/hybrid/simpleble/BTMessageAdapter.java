package ru.hybrid.simpleble;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by p.kuraev on 19.07.2017.
 */

public class BTMessageAdapter extends ArrayAdapter<BTMessage> implements ListAdapter {
    private static String LOG_TAG = BTMessageAdapter.class.getSimpleName();
    private ArrayList<BTMessage> listMessages;
    public static int MAX_ITEM_COUNT = 32;

    public BTMessageAdapter(@NonNull Context context,  @NonNull ArrayList<BTMessage> objects) {
        super(context, 0, objects);
        listMessages = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());
        convertView = inflater.inflate(R.layout.bt_message_list_item, parent, false);
        TextView address = (TextView)convertView.findViewById(R.id.address);
        TextView message = (TextView)convertView.findViewById(R.id.message);
        BTMessage device = getItem(position);
        address.setText(Integer.toString(device.getAddress()));
        message.setText(Float.toString(device.getData()));
        return convertView;
    }

    @Override
    public void add(@Nullable BTMessage object) {
        if (object.messageCrc16 != object.getCrcOfData()){
            Log.i(LOG_TAG, "Несовпадение CRC16");
            return;
        }

        if (listMessages.contains(object)){


            int position = listMessages.indexOf(object);;
            listMessages.set(position, object);

            Log.i(LOG_TAG, "Обновить значение для адреса: "  + Integer.toString(object.getAddress()));
        }
        else{
//            проверка на количество сообщений, если уже переполнено, то сдвиг

            if (listMessages.size() >= MAX_ITEM_COUNT){
//                удалить  самый старый елемент из массива, сдвинув все эелементы массива на индекс ниже
                Log.i(LOG_TAG, "Сдвиг индексов в листе для освобождения места для нового элемента");
                for (int i=0; i<= listMessages.size()-2; i=i+1){
                    listMessages.set(i, listMessages.get(i+1));
                }
                listMessages.remove(listMessages.size()-1);
            }
//            add(object);
            listMessages.add(object);

        }
        Log.i(LOG_TAG, "Notify dataset changed");
        notifyDataSetChanged();

    }
}
