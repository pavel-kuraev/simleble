package ru.hybrid.simpleble;


import android.util.Log;

public class BTMessageFactory{
    private static String LOG_TAG = BTMessageFactory.class.getSimpleName();
    static BTMessage getBtMessage(byte[] bytes){
        int btMessageLength = BTMessage.ADDRESS_LENGTH_BYTES + BTMessage.DATA_LENGTH_BYTES + BTMessage.CRC16_LENGTH_BYTES;
        if(bytes.length != btMessageLength){
            Log.w(LOG_TAG, "Unsupported messsges length");
            StringBuilder b = new StringBuilder();
            final StringBuilder stringBuilderMessageBytes = new StringBuilder(bytes.length);
            Log.w(LOG_TAG, "data bytes length-> " + Integer.toString(bytes.length));
            for (byte bb: bytes){
                stringBuilderMessageBytes.append(Integer.toString(bb) + " ");
                b.append(String.format("%02X ", bb));
            }
            Log.w(LOG_TAG, "data hex-> " + b.toString());
            Log.w(LOG_TAG, "data bytes-> " + stringBuilderMessageBytes.toString());
            return null;
        }
        else{
            String addressString =  String.format("%02X", bytes[0]);
            int address = hexToInt(addressString);
            final StringBuilder stringBuilderMessage = new StringBuilder(bytes.length);
            final StringBuilder stringBuilderMessageBytes = new StringBuilder(bytes.length);
            StringBuilder stringBuilderMessageCRC = new StringBuilder();
            for (int i=1; i<=BTMessage.DATA_LENGTH_BYTES; i=i+1){
                stringBuilderMessageBytes.append(Integer.toString(bytes[i]) + " ");
                stringBuilderMessage.append(String.format("%02X", bytes[i]));
            }
            for (int i=5; i<=6; i=i+1){
                stringBuilderMessageCRC.append(String.format("%02X", bytes[i]));
            }
            float floatDataValue = hexToFloat(stringBuilderMessage.toString());
            int messageCrc =  hexToInt(stringBuilderMessageCRC.toString());
            Log.i(LOG_TAG, "bytes count -> " + Integer.toString(bytes.length));
            Log.i(LOG_TAG, "address -> " + Integer.toString(address));
            Log.i(LOG_TAG, "data -> " + Float.toString(floatDataValue));
            Log.i(LOG_TAG, "data hex-> " + stringBuilderMessage.toString());
            Log.i(LOG_TAG, "Crc16 hex-> " + stringBuilderMessageCRC.toString());
            Log.i(LOG_TAG, "Crc16 int-> " + Integer.toString(messageCrc));
            Log.i(LOG_TAG, "data bytes length-> " + Integer.toString(bytes.length));
            Log.i(LOG_TAG, "data bytes-> " + stringBuilderMessageBytes.toString());
            BTMessage message = new BTMessage(bytes, address, floatDataValue, messageCrc);
            return message;
        }
    }


    public static float hexToFloat(String hexString){
        Long i = Long.parseLong(hexString, 16);
        Float f = Float.intBitsToFloat(i.intValue());
        return f;
    }

    public static int hexToInt(String hexString){
        int value = Integer.parseInt(hexString, 16);
        return value;
    }

}
